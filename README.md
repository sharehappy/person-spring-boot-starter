 **spring-boot 自定义starter  person-spring-boot-starter** 

#### 介绍
spring-boot 2.1.7.RELEASE自定义spring-boot-starter


1.创建spring-boot-starter-person maven项目和person-spring-boot-autoconfigure person-spring-boot-starter
person-spring-boot-autoconfigure person-spring-boot-starter都是spring-boot-starter-person的子模块
person-spring-boot-starter pom.xml中必须添加person-spring-boot-autoconfigure依赖
person-spring-boot-starter的pom.xml

```
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-autoconfigure</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-autoconfigure-processor</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-configuration-processor</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>
```
lombok为加载get set 方法的插件包

2.创建配置类

```
@ConfigurationProperties(prefix = "my.person")
@Getter
@Setter
@ToString
public class PersonProperties {
    private String name;
    private String address;
    private Integer age;
    private Date birth;
}
```

prefix的值即后面在其他spring-boot项目中可以添加的配置的前缀

3.创建核心服务类

```
@AllArgsConstructor
@NoArgsConstructor
public class PersonService {
    private PersonProperties personProperties;

    public void sayHello() {
        System.out.println("hello stater:" + personProperties.toString());
    }
}
```
sayHello()仅仅是一个测试方法
下面贴一下mybatis-spring-boot-starter 2.1.0的部分方法

```
@Configuration
@ConditionalOnClass(LanguageDriver.class)
public class MybatisLanguageDriverAutoConfiguration {

  private static final String CONFIGURATION_PROPERTY_PREFIX = "mybatis.scripting-language-driver";

  /**
   * Configuration class for mybatis-freemarker 1.1.x or under.
   */
  @Configuration
  @ConditionalOnClass(FreeMarkerLanguageDriver.class)
  @ConditionalOnMissingClass("org.mybatis.scripting.freemarker.FreeMarkerLanguageDriverConfig")
  public static class LegacyFreeMarkerConfiguration {
    @Bean
    @ConditionalOnMissingBean
    FreeMarkerLanguageDriver freeMarkerLanguageDriver() {
      return new FreeMarkerLanguageDriver();
    }
  }
｝
```


4.创建自动配置类
每个starter必须要有自动配置类

```
@Configuration
@ConditionalOnClass({PersonService.class})
@EnableConfigurationProperties(PersonProperties.class)
public class PersonAutoConfiguration {
    @Autowired
    PersonProperties personProperties;

    @Bean
    @ConditionalOnMissingBean(PersonService.class)
    public PersonService personService() {
        PersonService personService = new PersonService(personProperties);
        return personService;
    }
}
```


@ConditionalOnClass：当类路径classpath下有指定的类的情况下进行自动配置

@ConditionalOnMissingBean:当容器(Spring Context)中没有指定Bean的情况下进行自动配置

@ConditionalOnProperty(prefix = “example.service”, value = “enabled”, matchIfMissing = true)，当配置文件中example.service.enabled=true时进行自动配置，如果没有设置此值就默认使用matchIfMissing对应的值

@ConditionalOnMissingBean，当Spring Context中不存在该Bean时。

@ConditionalOnBean:当容器(Spring Context)中有指定的Bean的条件下

@ConditionalOnMissingClass:当类路径下没有指定的类的条件下

@ConditionalOnExpression:基于SpEL表达式作为判断条件

@ConditionalOnJava:基于JVM版本作为判断条件

@ConditionalOnJndi:在JNDI存在的条件下查找指定的位置

@ConditionalOnNotWebApplication:当前项目不是Web项目的条件下

@ConditionalOnWebApplication:当前项目是Web项目的条件下

@ConditionalOnResource:类路径下是否有指定的资源

@ConditionalOnSingleCandidate:当指定的Bean在容器中只有一个，或者在有多个Bean的情况下，用来指定首选的Bean

5.autoconfigure工程中resources目录下创建META-INF目录，创建spring.factories和spring-autoconfigure-metadata.properties文件
spring.factories
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
  com.share.autoconfigure.PersonAutoConfiguration
spring-autoconfigure-metadata.properties
com.share.autoconfigure.PersonAutoConfiguration.ConditionalOnClass=com.share.autoconfigure.PersonService
com.share.autoconfigure.PersonAutoConfiguration.Configuration=
com.share.autoconfigure.PersonAutoConfiguration=

6.mvn clean install 打包

7.创建测试项目，在测试项目中添加

```
<dependency>
    <groupId>com.share</groupId>
    <artifactId>person-spring-boot-starter</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
```



8.新项目中的application.properties中可以加
my.person.address=北京
my.person.age=30
            
jdbc ldap redis solr aop jpa等的autoconfigure都在spring-boot-autoconfigure下面
demo测试项目地址：https://gitee.com/sharehappy/learn/tree/master/springboot_demo

参考：
https://docs.spring.io/spring-boot/docs/2.1.7.RELEASE/reference/pdf/spring-boot-reference.pdf

mybatis-spring-boot-starter 2.1.0
![输入图片说明](https://images.gitee.com/uploads/images/2019/0830/200408_f8fd3a96_394236.png "mybatis.png")

有问题可以加QQ群513650703讨论