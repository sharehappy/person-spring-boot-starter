package com.share.autoconfigure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * author:caifan
 * date:2019/8/29
 */
@Configuration
@ConditionalOnClass({PersonService.class})
@EnableConfigurationProperties(PersonProperties.class)
public class PersonAutoConfiguration {
    @Autowired
    PersonProperties personProperties;

    @Bean
    @ConditionalOnMissingBean(PersonService.class)
    public PersonService personService() {
        PersonService personService = new PersonService(personProperties);
        return personService;
    }
}
