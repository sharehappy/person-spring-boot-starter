package com.share.autoconfigure;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Date;

/**
 * author:caifan
 * date:2019/8/29
 */
@ConfigurationProperties(prefix = "my.person")
@Getter
@Setter
@ToString
public class PersonProperties {
    private String name;
    private String address;
    private Integer age;
    private Date birth;
}
