package com.share.autoconfigure;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class PersonService {
    private PersonProperties personProperties;

    public void sayHello() {
        System.out.println("hello stater:" + personProperties.toString());
    }
}
